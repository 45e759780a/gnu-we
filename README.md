# GNU-WE

GNU For Windows & Extras

> Terminal/console based binaries for Windows. Built to last. 32bit only.

## Abstract

## Content

#### Core

* ls, cat, cksum, dd, date, kill, m5sum, shred, sleep, wc, cp, mv, head, tail, uniq, sort, uptime

---

* Archiving/Compression - tar, bsdtar, unrar, zip, gzip, lha, bz2
* Encryption/Encoding/Gen - gpg, puttygen, 
* Networking/Parsing - wget, curl, pup, jq, scp [pscp], ssh [putty], sftp [psftp]
* Text Processing - grep, awk, gawk, sed, ed
* Development Tools - make, gcc, g++, gdb, git, groff, latex?
* Miscellaneous - calc, gcal

#### Software

* Text Editor - nano & vim
* Text Reader - less & more
* PDF Reader - foxit [GUI]
* Resource Monitor - ntop?
* Batch Aliases - see Scripts/

---

* File Manager - mc/tc
* Image Viewer - imageglass
* Music Player - mpxp
* Video Player - vlc & ffmpeg
* Networking - rsync, wget, curl, ssh [putty], sftp [psftp], etc

---

* Torrent Client - rtorrent
* IRC Client - irssi / weechat
* Email Client - mutt / emutt
* Password Manager - pass?
* RSS Client - newsboat

## Versions

## Source Origin
